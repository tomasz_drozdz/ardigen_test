import csv
from functools import reduce
import unittest


def read_data(currencies_file, matchings_file, data_file):
    currencies = {}
    with open(currencies_file, newline='') as f:
        for od in csv.DictReader(f):
            currencies[od['currency']] = float(od['ratio'])

    matchings = {}
    with open(matchings_file, newline='') as f:
        for od in csv.DictReader(f):
            matchings[od['matching_id']] = int(od['top_priced_count'])

    data = []
    with open(data_file, newline='') as f:

        for od in csv.DictReader(f):
            price = float(od['price'])
            currency = od['currency']
            quantity = int(od['quantity'])
            matching_id = int(od['matching_id'])

            data.append({
                'price': price,
                'currency': currency,
                'quantity': quantity,
                'matching_id': matching_id})


    return (currencies, matchings, data)


currencies, matchings, data = read_data("./currencies.csv", "./matchings.csv", "./data.csv")



def valuation(currencies, matchings, data):
    _data = []
    for d in data:
        total_price_in_PLN = d['quantity'] * d['price'] * currencies[d['currency']]

        _data.append({
            'quantity': d['quantity'],
            'matching_id': d['matching_id'],
            'total_price_in_PLN': total_price_in_PLN})

    top_products = []

    for matching_id, top_priced_count in matchings.items():
        matching_id = int(matching_id)

        matching_data = list(filter(lambda o: o['matching_id'] == matching_id, _data))
        matching_data.sort(key=lambda o: o['total_price_in_PLN'], reverse=True)

        matching_top_products = matching_data[:top_priced_count]
        ignored_products = matching_data[top_priced_count:]

        matching_top_products_count = reduce(lambda x, y: x + y, map(lambda o: o['quantity'], matching_top_products))
        matching_top_products_total_price = reduce(lambda x, y: x + y, map(lambda o: o['total_price_in_PLN'], matching_top_products))
        matching_top_products_avg_price = matching_top_products_total_price / matching_top_products_count
        matching_top_products_matching_id = matching_id
        matching_top_products_currency = 'PLN'

        if ignored_products:
            ignored_products_count = reduce(lambda x, y: x + y, map(lambda o: o['quantity'], ignored_products))
        else:
            ignored_products_count = 0

        top_products.append({
            'matching_id': matching_top_products_matching_id,
            'total_price': matching_top_products_total_price,
            'avg_price': matching_top_products_avg_price,
            'currency': matching_top_products_currency,
            'ignored_products_count': ignored_products_count})

    return top_products



top_products = valuation(currencies, matchings, data)

with open("./top_products.csv", mode="wt", newline="") as f:
    w = csv.DictWriter(f, fieldnames=['matching_id', 'total_price', 'avg_price', 'currency', 'ignored_products_count'])
    w.writeheader()
    w.writerows(top_products)



class ValuationTestCase(unittest.TestCase):
    def test_valuation(self):
        matchings = {'1': 2, '2': 2, '3': 3}
        currencies = {'GBP': 2.4, 'EU': 2.1, 'PLN': 1.0}
        data = [
            {'price': 1000.0, 'currency': 'GBP', 'quantity': 2, 'matching_id': 3},
            {'price': 1050.0, 'currency': 'EU', 'quantity': 1, 'matching_id': 1},
            {'price': 2000.0, 'currency': 'PLN', 'quantity': 1, 'matching_id': 1},
            {'price': 1750.0, 'currency': 'EU', 'quantity': 2, 'matching_id': 2},
            {'price': 1400.0, 'currency': 'EU', 'quantity': 4, 'matching_id': 3},
            {'price': 7000.0, 'currency': 'PLN', 'quantity': 3, 'matching_id': 2},
            {'price': 630.0, 'currency': 'GBP', 'quantity': 5, 'matching_id': 3},
            {'price': 4000.0, 'currency': 'EU', 'quantity': 1, 'matching_id': 3},
            {'price': 1400.0, 'currency': 'GBP', 'quantity': 3, 'matching_id': 1}]

        top_products = valuation(currencies, matchings, data)
        expected_result = [
            {'matching_id': 1, 'total_price': 12285.0, 'avg_price': 3071.25, 'currency': 'PLN', 'ignored_products_count': 1},
            {'matching_id': 2, 'total_price': 28350.0, 'avg_price': 5670.0, 'currency': 'PLN', 'ignored_products_count': 0},
            {'matching_id': 3, 'total_price': 27720.0, 'avg_price': 2772.0, 'currency': 'PLN', 'ignored_products_count': 2}]
        self.assertEqual(top_products, expected_result)
