def fiz_buzz(n=1, m=10000):
    if n < 1:
        raise AttributeError("n has to be >= 1")
    if m > 10000:
        raise AttributeError("m has to be <= 10000")
    if n >= m:
        raise AttributeError("n has to be < m")

    for _i in range(n, m + 1):
        if not _i % 3 and not _i % 5:
            yield "FizzBuzz"
        elif not _i % 3:
            yield "Fizz"
        elif not _i % 5:
            yield "Buzz"
        else:
            yield _i

for i in fiz_buzz():
    print(i)
